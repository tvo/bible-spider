#!/usr/bin/env python3
from lxml import html
import requests

def filterVersions(input):
    return input.isdigit()

class Book:
    name = ""
    version = ""
    chapterCount = 0

    def __init__(self, n, v):
        self.name = n
        self.version = v


    def get_chapters(self):
        n = self.name.replace(' ', '-')
        url = 'https://www.biblestudytools.com/' + self.version + '/' + n
        page = requests.get(url)
        versionContent = html.fromstring(page.content)
        tags = versionContent.xpath('//a')
        tags = list(map(lambda x : str(x.text).strip(), tags))
        tags = list(filter(filterVersions, tags))
        chapters = []
        for element in tags:
            chapters.append(int(element))
        self.chapterCount = max(chapters)
        print(self.chapterCount)

    def get_chapter_text(self, n):
        url = 'https://www.biblestudytools.com/esv/' + self.name.replace(' ', '-')
        url += '/' + str(n) + '.html'
        page = requests.get(url)
        versionContent = html.fromstring(page.content)
        tags = versionContent.xpath('//div[@class="scripture verse-padding"]/div')

        for bad in  versionContent.xpath("//a[(contains(@href,'javascript'))]"):
            bad.getparent().remove(bad)
        verses = []
        for t in tags:
            verses.append(' '.join(t.text_content().strip().split()))
        return verses
