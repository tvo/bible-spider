#!/usr/bin/env python3
import os
from lxml import html, etree
import requests
import re
from book import Book

def get_all_versions():
    page = requests.get('https://www.biblestudytools.com/bible-versions/')
    versionContent = html.fromstring(page.content)
    tags = versionContent.xpath('//a')

    def filterVersions(input):
        input = str(input)
        if not re.findall('https://www.biblestudytools.com/[a-z]{2,5}/$', input):
            return False
        if not re.findall('(video|blogs)', input):
            return True
        return False

    def reduceToHref(input):
        return input.get('href')

    tags = list(map(lambda x : x.get('href'), tags))
    tags = list(filter(filterVersions, tags))
    tags.sort()
    blackListedVersions = [ 'aa', 'bla', 'cuv', 'cuvp', 'cuvs', 'elb', 'gdb',
                           'jbs', 'lsg', 'lut', 'ntv', 'nvi', 'nvip', 'ost',
                           'riv', 'rvr', 'sev', 'svv' ]
    versions = []
    for element in tags:
        version = element[32:].replace('/', '')
        if version not in blackListedVersions:
            versions.append(version)
    return versions

def filterChapterVersions(input):
    input = str(input)
    if not re.findall('(video|blogs)', input):
        return True
    return False

def get_book_for_version(version):
    page = requests.get('https://www.biblestudytools.com/' + version + '/')
    versionContent = html.fromstring(page.content)
    tags = versionContent.xpath('//h4')
    tags = list(map(lambda x : html.tostring(x, method='text').strip(), tags))
    tags = list(filter(lambda x : not re.findall('(video|blogs)', str(x)), tags))
    books = []
    for element in tags:
        books.append(str(element.decode('utf8')))
    return books

def dirExists(path):
    return os.path.exists(path)

def makedir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def writeFile(path, title, lines):
    with open(path, 'w') as f:
        f.write(title)
        f.write('\n')
        for line in lines:
            f.write(line)
            f.write('\n')

def create_books(versions):
    books = []
    for version in versions:
        try:
            bookNames = get_book_for_version(version)
            for bookName in bookNames:
                books.append(Book(bookName, version))
        except:
            print('failed for:')
            print(version)
    return books

def bookChapters(books):
    bc = []
    for book in books:
        chapters = book.get_chapters()
        for c in chapters:
            bc.append((book, chapter))
    return bc

if __name__ == '__main__':
    baseDir = 'text'
    makedir(baseDir)
    books = create_books(get_all_versions())

    # make all the dirs for the versions
    for b in books:
        makedir(baseDir + '/' + b.version)

    bookChapters = bookChapters(books)
    for (book, chapter) in bookChapters:
        makdir(baseDir + '/' + book.version + '/' + chapter)

    print(map(lambda x : x.name))
    exit(0)

    for version in versions:
        dir = "text/" + version
        makedir(dir)
        print(version)
        books = get_book_for_version(version)
        for bookName in books:
            dir2 = dir
            dir2 += '/' + bookName
            if dirExists(dir2):
                continue
            makedir(dir2)
            print(bookName)
            book = Book(bookName, version)

            book.get_chapters()
            for c in range(1, book.chapterCount + 1):
                title = bookName + ' '
                title += str(c) + ' - '
                title += version
                text = book.get_chapter_text(c)
                print(c)
                writeFile(dir2 + '/' + str(c) + '.txt', title, text)
