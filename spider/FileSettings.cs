using System.IO;

namespace Spider
{
	public record FileSettings
	{
		public FileSettings(string basePath = null, string version = null, string book = null, string chapter = null)
		{
			if (string.IsNullOrWhiteSpace(basePath))
				basePath = Path.Combine(Directory.GetCurrentDirectory(), "data");
			if (!Directory.Exists(basePath))
				Directory.CreateDirectory(basePath);
			BasePath = basePath;

			if (string.IsNullOrWhiteSpace(version))
				version = "version.txt";
			VersionFileName = version;

			if (string.IsNullOrWhiteSpace(book))
				book = "books";
			BookDirectory = book;

			if (string.IsNullOrWhiteSpace(chapter))
				chapter = "chapters";
			ChapterDirectory = chapter;
		}

		public readonly string BasePath;
		public readonly string VersionFileName;
		public readonly string BookDirectory;
		public readonly string ChapterDirectory;

		public string GetChapterDirectory(Models.Book book) =>
			Path.Combine(
					BasePath,
					ChapterDirectory,
					book.Version.Identifier.ToLower(),
					book.SafeName);


	}
}
