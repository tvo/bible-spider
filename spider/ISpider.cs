using System.Threading.Tasks;
using System.Collections.Immutable;

namespace Spider
{
	public interface ISpider
	{
		Task<ImmutableList<string>>QueryText(string uri, string xpath);
	}
}
