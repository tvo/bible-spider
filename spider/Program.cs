﻿using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using System.Linq;
using Autofac;

namespace Spider
{
	using Models;
	using Repository.Version;
	using Repository.Book;
	using Repository.Chapter;

    class Program
    {
        static async Task Main(string[] args)
        {
			var sp = diSetup.Setup().Build();
			var vRepo = sp.Resolve<IVersionRepository>();
			var bRepo = sp.Resolve<IBookRepository>();
			var cRepo = sp.Resolve<IChapterRepository>();

			var versions = await vRepo.Versions();
			var books = ImmutableList<Book>.Empty;
			foreach(var version in versions.Take(1))
				books = books.AddRange(await bRepo.GetBooks(version));
			var f = books.First();
			var text = await cRepo.GetChapter(f, 1);

			Console.WriteLine("fin.");
        }
    }
}
