using System.IO;
using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Linq;
using Newtonsoft.Json;

namespace Spider.Repository.Book
{
	using Models;

	public class BookFileDecorator : IBookRepository
	{
		IBookRepository repository;
		FileSettings settings;

		public BookFileDecorator(IBookRepository repository, FileSettings fileSettings)
		{
			this.repository = repository;
			settings = fileSettings;
		}

		public async Task<ImmutableList<Book>> GetBooks(Version version)
		{
			var bookDir = Path.Combine(settings.BasePath, settings.BookDirectory);
			if(!Directory.Exists(bookDir))
				Directory.CreateDirectory(bookDir);

			var path = Path.Combine(bookDir, version.Identifier.ToLower());
			if(File.Exists(path))
			{
				var lines = await File.ReadAllLinesAsync(path);
				var books = ImmutableList<Book>.Empty;
				foreach(var line in lines)
				{
					books = books.Add(JsonConvert.DeserializeObject<Book>(line));
				}
				return books;
			}

			var result = await repository.GetBooks(version);
			// cache the result here.
			await File.WriteAllLinesAsync(path, result.Select(x => JsonConvert.SerializeObject(x)));
			return result;
		}
	}
}
