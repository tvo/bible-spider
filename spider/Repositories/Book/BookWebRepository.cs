using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Immutable;
using System.Text.RegularExpressions;

namespace Spider.Repository.Book
{
	using Models;

	public class BookWebRepository : IBookRepository
	{
		private readonly ISpider spider;

		public BookWebRepository(ISpider spider)
		{
			this.spider = spider;
		}

		public static Regex NumberRegex = new Regex("^[0-9]{1,3}$", RegexOptions.Compiled);

		public async Task<int> ChapterCount(Version version, string bookName)
		{
			bookName = bookName.Replace(' ', '-');
			Console.WriteLine($"Getting chapter count for book: {bookName} {version.Identifier}");
			var url = $"https://www.biblestudytools.com/{version.Identifier.ToLower()}/{bookName}/";
			var items = (await spider.QueryText(url, "*[xpath>'//a[contains(@class,\"btn\")]']")).AsEnumerable();
			items = items.Select(x => x.Trim()).Where(x => NumberRegex.Match(x).Success);
			var number = items
				.Select(x => int.TryParse(x, out var n) ? n : -1)
				.Where(x => x > -1)
				.Max();

			return number;
		}

		public async Task<ImmutableList<Book>> GetBooks(Version version)
		{
			Console.WriteLine("Getting books for version: " + version.Identifier);

			var url = $"https://www.biblestudytools.com/{version.Identifier}/";
			var items = await spider.QueryText(url, "h4");
			var result = ImmutableList<Book>.Empty;

			foreach(var item in items)
			{
				var count = await ChapterCount(version, item);
				result = result.Add(new Book(item, count, version));
			}
			return result;
		}
	}
}
