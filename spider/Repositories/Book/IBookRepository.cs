using System.Threading.Tasks;
using System.Collections.Immutable;

namespace Spider.Repository.Book
{
	using Models;

	public interface IBookRepository
	{
		Task<ImmutableList<Book>> GetBooks(Version version);
	}
}
