using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Spider.Repository.Chapter
{
	using Models;

	public class ChapterFileDecorator : IChapterRepository
	{
		IChapterRepository repository;
		FileSettings settings;

		public ChapterFileDecorator(IChapterRepository repository, FileSettings fileSettings)
		{
			this.repository = repository;
			settings = fileSettings;
		}

		public async Task<Chapter> GetChapter(Book book, int chapterNumber)
		{
			var chapterDir = settings.GetChapterDirectory(book);
			if(!Directory.Exists(chapterDir))
				Directory.CreateDirectory(chapterDir);

			var path = Path.Combine(chapterDir, chapterNumber.ToString() + ".json");
			if(File.Exists(path))
			{
				var text = await File.ReadAllTextAsync(path);
				return JsonConvert.DeserializeObject<Chapter>(text);
			}

			var result = await repository.GetChapter(book, chapterNumber);

			// cache the result here.
			var chapterText = JsonConvert.SerializeObject(result);
			await File.WriteAllTextAsync(path, chapterText);
			return result;
		}
	}
}
