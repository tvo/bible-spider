using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Spider.Repository.Chapter
{
	using Models;

	public class ChapterWebRepository : IChapterRepository
	{
		private readonly ISpider spider;

		public ChapterWebRepository(ISpider spider)
		{
			this.spider = spider;
		}

		public async Task<Chapter> GetChapter(Book book, int chapterNumber)
		{
			var version = book.Version.Identifier.ToLower();
			var url = $"https://www.biblestudytools.com/{version}/{book.SafeName}/{chapterNumber}.html";

			Console.WriteLine($"Getting chapter text: {book.SafeName}:{chapterNumber} - ({version})");

			var items = (await spider.QueryText(url, "*[xpath>'//div[contains(@class,\"scripture\")]/div/span']"))
				.AsEnumerable();
			items = items.Select(x => x.Trim());
			// combine the number span and the text span.
			var result = new List<string>();
			var enumerator = items.GetEnumerator();
			while(enumerator.MoveNext())
			{
				var one = enumerator.Current;
				if(!enumerator.MoveNext())
					break;
				var two = enumerator.Current;
				result.Add(one + " " + two); // Do we want the verse number at the start of each line?
			}

			return new Chapter(book, chapterNumber, result);
		}
	}
}
