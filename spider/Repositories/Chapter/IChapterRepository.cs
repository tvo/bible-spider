using System.Threading.Tasks;

namespace Spider.Repository.Chapter
{
	using Models;

	public interface IChapterRepository
	{
		Task<Chapter> GetChapter(Book book, int chapter);
	}
}
