using System.Threading.Tasks;
using System.Collections.Immutable;

namespace Spider.Repository.Version
{
	using Models;

	public interface IVersionRepository
	{
		public Task<ImmutableList<Version>> Versions();
	}
}
