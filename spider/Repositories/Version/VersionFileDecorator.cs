using System.IO;
using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Linq;

namespace Spider.Repository.Version
{
	using Models;

	public class VersionFileDecorator : IVersionRepository
	{
		IVersionRepository repository;
		FileSettings settings;

		public VersionFileDecorator(IVersionRepository repository, FileSettings fileSettings)
		{
			this.repository = repository;
			settings = fileSettings;
		}

		public async Task<ImmutableList<Version>> Versions()
		{
			var path = Path.Combine(settings.BasePath, settings.VersionFileName);
			if(File.Exists(path))
			{
				var lines = await File.ReadAllLinesAsync(path);
				return ImmutableList<Version>.Empty.AddRange(lines.Select(x => new Version(x)));
			}
			var result = await repository.Versions();
			// cache the result here.
			await File.WriteAllLinesAsync(path, result.Select(x => x.Identifier));
			return result;
		}
	}
}
