using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Spider.Repository.Version
{
	using Models;

	public class VersionWebRepository : IVersionRepository
	{
		ISpider spider;

		public VersionWebRepository(ISpider spider)
		{
			this.spider = spider;
		}

		public static Regex TextRegex = new Regex("^[A-Z]{2,5}$", RegexOptions.Compiled | RegexOptions.Multiline);

		public static readonly ImmutableList<string> BlackList = ImmutableList<string>.Empty.AddRange(
				new []{
					"aa", "bla", "cuv", "cuvp", "cuvs", "elb", "gdb",
					"jbs", "lsg", "lut", "ntv", "nvi", "nvip", "ost",
					"riv", "rvr", "sev", "svv"
				});

		public async Task<ImmutableList<Version>> Versions()
		{
			System.Console.WriteLine("Making web requests for versions.");
			var url = "https://www.biblestudytools.com/bible-versions/";
			IEnumerable<string> textLinks = await spider.QueryText(url, "a");

			// filter all the link texts with what we expect the bible versisons should be.
			textLinks = textLinks.Where(x => !string.IsNullOrWhiteSpace(x))
				.SelectMany(x => x.Split(' '))
				.Select(x => x.Trim())
				;
			textLinks = textLinks.Where(x => TextRegex.Match(x).Success);
			var versions = textLinks.Select(x => new Models.Version(x));

			return ImmutableList<Version>.Empty.AddRange(versions);
		}
	}
}
