using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;

namespace Spider
{
	public class SpiderWorker : ISpider
	{
		public async Task<ImmutableList<string>> QueryText(string url, string xpath)
		{
			var config = Configuration.Default.WithDefaultLoader().WithXPath();
            var context = BrowsingContext.New(config);
            var document = await context.OpenAsync(url);
			var selector = document.QuerySelectorAll(xpath);

			return ImmutableList<string>.Empty.AddRange(selector.Select(x => x.TextContent));
		}
	}
}
