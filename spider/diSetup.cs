using Autofac;

namespace Spider
{
	using Repository.Version;
	using Repository.Book;
	using Repository.Chapter;

	public static class diSetup
	{
		public static ContainerBuilder Setup(ContainerBuilder services = null)
		{
			services = services ?? new ContainerBuilder();

			// defaults for file settings.
			services.RegisterType<FileSettings>().SingleInstance();

			services.RegisterType<SpiderWorker>().As<ISpider>();

			// version service and decorator
			services.RegisterType<VersionWebRepository>().As<IVersionRepository>();
			services.RegisterDecorator<VersionFileDecorator, IVersionRepository>();

			// book service and decorator
			services.RegisterType<BookWebRepository>().As<IBookRepository>();
			services.RegisterDecorator<BookFileDecorator, IBookRepository>();

			// book service and decorator
			services.RegisterType<ChapterWebRepository>().As<IChapterRepository>();
			services.RegisterDecorator<ChapterFileDecorator, IChapterRepository>();

			return services;
		}
	}
}
