namespace Spider.Models
{
	public record Book
	{
		public Book(string name, int chapters, Version version)
		{
			Name = name;
			Version = version;
			ChapterCount = chapters;
		}
		public string Name;
		public int ChapterCount;
		public Version Version;

		public string SafeName => Name.Replace(' ', '-');
	}
}
