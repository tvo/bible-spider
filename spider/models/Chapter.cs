using System.Collections.Generic;
using System.Collections.Immutable;

namespace Spider.Models
{
	public record Chapter
	{
		public Chapter(Book book, int chapterNumber, IEnumerable<string> chapterText)
		{
			Book = book;
			Number = chapterNumber;
			Text = Text.AddRange(chapterText);
		}

		public Book Book;
		public int Number;
		public ImmutableList<string> Text = ImmutableList<string>.Empty;
	}
}
