namespace Spider.Models
{
	public record Version
	{
		public Version(string id)
		{
			Identifier = id;
		}
		public string Identifier;
	}
}
